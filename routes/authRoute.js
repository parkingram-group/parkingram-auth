const router = require('express').Router();
const { loginUser } = require('../controllers/userController');

router.post('/login', loginUser);

module.exports = router;
