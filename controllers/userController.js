const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { User } = require('../database');

exports.loginUser = (req, res) => {
  const { username, password } = req.body;
  User.findOne({ where: { username } }).then((user, error) => {
    if (error) {
      res.status(500).send('Internal Error!');
    } else if (user === null) {
      res.status(404).send('User not found!');
    } else {
      const isPasswordCorrect = bcrypt.compareSync(password, user.dataValues.password);
      if (!isPasswordCorrect) {
        res.status(401).send('Password is not valid!');
      } else {
        const expiresIn = 60 * 60;
        const accessToken = jwt.sign({ id: user.dataValues.id }, process.env.SECRET_KEY, {
          expiresIn,
        });
        res.status(200).send({ access_token: accessToken, expires_in: expiresIn });
      }
    }
  });
};
