const express = require('express');
const cors = require('cors');
const logger = require('morgan');
const bcrypt = require('bcryptjs');
const database = require('./database');
const authRoute = require('./routes/authRoute');

const app = express();
app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(authRoute);

const port = normalizePort(process.env.PORT || '3000');

database.sequelize.sync().then(() => {
  app.listen(port, () => {
    const administrators = JSON.parse(process.env.ADMINISTRATORS);
    for (let i = 0; i < administrators.length; i += 1) {
      database.User.findOrCreate({
        where: {
          username: administrators[i].username,
          password: bcrypt.hashSync(administrators[i].password),
        },
      });
    }
  });
});

function normalizePort(val) {
  const port = parseInt(val, 10);

  // eslint-disable-next-line no-restricted-globals
  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}
