# parkingram-auth

# Installation

- `npm install`
- The following environment variables must be provided:
  - `DATABASE_URL` (the URL of the PostgreSQL database)
  - `SECRET_KEY` (for the JWT)
  - `ADMINISTRATORS` (MUST be a JSON array, like so: `[{"username": "first username", "password": "first password"}]`)
- `npm start`
